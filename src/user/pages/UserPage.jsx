import React from 'react';

import UsersList from '../components/usersList/UsersList';

const UserPage = () => {
  //Simulación de usuarios, debido a que no hay backend todavía
  const USERS = [
    {
      id: 'u1',
      name: 'Monkey D. Luffy',
      image: 'https://cdn.alfabetajuega.com/wp-content/uploads/2019/03/luffy-llorando-780x405.jpg',
      places: 2
    },
    {
      id: 'u2',
      name: 'Roronoa Zoro',
      image: 'https://d3b4rd8qvu76va.cloudfront.net/664/115/244/-299996995-1tc1c83-ethtdgps68400kp/original/11037010_868964169823336_2552191692560480237_o.jpg',
      places: 3
    }
  ];

  return (
    <UsersList
      items={USERS}
    />
  );
};

export default UserPage;