import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import UserPage from './user/pages/UserPage';
import NewPlacePage from './places/pages/NewPlacePage';
import MainNavigation from "./shared/components/Navigation/MainNavigation";

export default function App() {
  return (
    <Router>
      <MainNavigation />
      <main>
        <Switch>
          <Route path="/" exact>
            <UserPage />
          </Route>
          <Route path="/places/new" exact>
            <NewPlacePage />
          </Route>
          <Redirect to="/" />
        </Switch>
      </main>
    </Router>
  );
}
