import React from 'react';
import { NavLink } from 'react-router-dom';

import './NavLinks.css';

const NavLinks = () => {

  return (
    <ul className="nav-links" >
      <li>
        <NavLink to="/" exact>Todos los usuarios</NavLink>
      </li>
      <li>
        {/* Está hardcodeado el lugar, hasta que realizemos el backend */}
        <NavLink to="/u1/places">Mis lugares</NavLink>
      </li>
      <li>
        <NavLink to="/places/new">Añadir lugar</NavLink>
      </li>
      <li>
        {/* Este path lo crearemos lo último, ya que no aporta valor al usuario, pero quiero colocar el link para no olvidarnos */}
        <NavLink to="/auth">Identificarse</NavLink>
      </li>
    </ul>
  );
};

export default NavLinks;