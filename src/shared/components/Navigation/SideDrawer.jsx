import React from 'react';
import ReactDom from 'react-dom';
import { CSSTransition } from 'react-transition-group';

import './SideDrawer.css';

const SideDrawer = (props) => {
  const { children, show, onClick } = props;
  const content = (
    <CSSTransition
      in={show}
      timeout={200}
      classNames={{
        enterActive: "slide-in-left-enter",
        enterDone: "slide-in-left-enter-active",
        exitActive: "slide-in-left-exit",
        exit: "slide-in-left-exit-active"
      }}
      mountOnEnter
      unmountOnExit
    >
      <aside className="side-drawer" onClick={onClick} > {children}</aside>
    </CSSTransition>
  );

  return ReactDom.createPortal(content, document.getElementById('drawer-hook'));
};

export default SideDrawer;